#pragma GCC diagnostic ignored "-Wwrite-strings" //To ignore string conversion warnings

// #include <python2.7/Python.h>  //Think this was for the wifi printer
#include <iostream>  //for prints
#include <stdio.h>   //for sprintf
#include <string.h>  // for strlen
#include <string>
#include <fstream>   // for ifstream, ofstream
#include <unistd.h>  //for usleep
#include <stdlib.h>  // for system
#include <thread>   // for thread
#include <atomic>   // for atomic_bool


#include "keypad.h" // For the keypad
#include "lcd.h" // For the LCD
#include "gpio_lib.h"  // for gpio functions

#define LED_R SUNXI_GPD(19)
#define LED_G SUNXI_GPD(20)
#define LED_B SUNXI_GPD(21)

// Commented out returns to match previous code
#define GPIO_CFG(x,y)  { \
																if(SETUP_OK!=sunxi_gpio_set_cfgpin(x,y)) {  \
																								cout << "Failed to config GPIO pin\n"; \
     \
																} \
}
#define GPIO_OUT(x,y)  { \
																if(sunxi_gpio_output(x,y)) {  \
																								cout << "Failed to set GPIO pin value\n"; \
     \
																} \
}
#define GPIO_IN sunxi_gpio_input

using namespace std;

/*************************************************************************
   // =======================================================================
   //
   //		Menu Class
   //
   //
   // =======================================================================
*************************************************************************/
class Menu
{
LCD lcd;       //Instantiate LCD class
Keypad keypad;      //Instantiate Keypad Class

int screen_update;    //Variable to trigger screen updates, prevents screen flickering, program will still work without this.
char key;        // indicates the key pressed, used for key presses
int row;
int col;

void (Menu::*foo)();

public:

Menu();     //Constructor
~Menu();    //Destructor

// Menu Screen Defined below

void Hibernate();
void main_loop();

};

// Constructor
// Sets up GPIOs, Turns on LEDs and reads config file for settings saved previously
Menu::Menu(){

								//load images
								system("od -N 36864 -j 138 -t x1 -v -w384 ./bitmaps/demo_background.bmp > ./bitmaps/idle.txt");
								lcd.Init_fullBMP("./bitmaps/idle.txt", "Idle");

								lcd.lcd_clr_all();
								lcd.write_fullBMP ("Idle");

								//update screen to hibernate bitmap
								//screen_update = 1;

								screen_update = 0;
								key=0;
								row=0;
								col=0;

								if(SETUP_OK!=sunxi_gpio_init())
								{
																cout << "Failed to initialize GPIO\n";
																return;
								}

								GPIO_CFG(LED_R,OUTPUT);
								GPIO_CFG(LED_G,OUTPUT);
								GPIO_CFG(LED_B,OUTPUT);
								//GPIO_CFG(AUDIO_P,OUTPUT);

								GPIO_OUT(LED_R,1);
								GPIO_OUT(LED_G,1);
								GPIO_OUT(LED_B,0);
								//GPIO_OUT(AUDIO_P,1);

}

// Destructor
Menu::~Menu(){

								sunxi_gpio_cleanup();

}


/*************************************************************************
   // =======================================================================
   //
   //		Menu Screens Start below
   //
   //
   // =======================================================================
*************************************************************************/


//wait for key press
void Menu::Hibernate()
{
								if(screen_update==0)
								{

																//clear lcd
																lcd.lcd_clr_all();
																lcd.write_fullBMP ("Idle");

																screen_update=1;
								}

								//checks key presses
								key = keypad.check();

								if (key) // any key to return to simplemenu
								{
																screen_update=0;
																//close hibernatemenu, load simplemenu etc.
																system("bash /home/wiApp/System/simple_load.sh");
								}
								usleep(10000);
}

void Menu::main_loop()
{

								lcd.lcd_clr_all();

								foo=&Menu::Hibernate;
								screen_update=0;

								while(true)
								{
																(this->*foo)();
								}
								return;
}

int main()
{
								Menu menu;
								menu.main_loop();
								return 0;
}
