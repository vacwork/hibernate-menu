
#pragma GCC diagnostic ignored "-Wwrite-strings" //To ignore string conversion warnings

// #include <python2.7/Python.h>  //Think this was for the wifi printer
#include <iostream>  //for prints
#include <stdio.h>   //for sprintf
#include <string.h>  // for strlen
#include <string>
#include <fstream>   // for ifstream, ofstream
#include <unistd.h>  //for usleep
#include <stdlib.h>  // for system
#include <thread>   // for thread
#include <atomic>   // for atomic_bool

//For sockets
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
//end
#include "keypad.h" // For the keypad
#include "lcd.h" // For the LCD
#include "zcam.h"
#include "gpio_lib.h"  // for gpio functions

/* OpenSSL headers */
#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#define LED_R SUNXI_GPD(19)
#define LED_G SUNXI_GPD(20)
#define LED_B SUNXI_GPD(21)
#define AUDIO_P SUNXI_GPD(16)

// Commented out returns to match previous code
#define GPIO_CFG(x,y)  { \
																if(SETUP_OK!=sunxi_gpio_set_cfgpin(x,y)) {  \
																								cout << "Failed to config GPIO pin\n"; \
     \
																} \
}
#define GPIO_OUT(x,y)  { \
																if(sunxi_gpio_output(x,y)) {  \
																								cout << "Failed to set GPIO pin value\n"; \
     \
																} \
}
#define GPIO_IN sunxi_gpio_input

using namespace std;

/***********************************************************************

   SSL stuff from http://www.chesterproductions.net.nz/blogs/it/c/an-ssl-client-using-openssl/245/

 **************************************************************************/
/**
 * Simple log function
 */
void slog(char* message)
{
								fprintf(stdout, message);
}

/**
 * Print SSL error details
 */
void print_ssl_error(char* message, FILE* out)
{

								fprintf(out, message);
								fprintf(out, "Error: %s\n", ERR_reason_error_string(ERR_get_error()));
								fprintf(out, "%s\n", ERR_error_string(ERR_get_error(), NULL));
								ERR_print_errors_fp(out);
}

/**
 * Print SSL error details with inserted content
 */
void print_ssl_error_2(char* message, char* content, FILE* out)
{

								fprintf(out, message, content);
								fprintf(out, "Error: %s\n", ERR_reason_error_string(ERR_get_error()));
								fprintf(out, "%s\n", ERR_error_string(ERR_get_error(), NULL));
								ERR_print_errors_fp(out);
}

/**
 * Initialise OpenSSL
 */
void init_openssl()
{

								/* call the standard SSL init functions */
								SSL_load_error_strings();
								SSL_library_init();
								ERR_load_BIO_strings();
								OpenSSL_add_all_algorithms();

								/* seed the random number system - only really nessecary for systems without '/dev/random' */
								/* RAND_add(?,?,?); need to work out a cryptographically significant way of generating the seed */
}

/**
 * Close an unencrypted connection gracefully
 */
int close_connection(BIO* bio)
{

								int r = 0;

								r = BIO_free(bio);
								if (r == 0) {
																/* Error unable to free BIO */
								}

								return r;
}

/**
 * Connect to a host using an unencrypted stream
 */
BIO* connect_unencrypted(char* host_and_port) {

								BIO* bio = NULL;

								/* Create a new connection */
								bio = BIO_new_connect(host_and_port);
								if (bio == NULL) {

																print_ssl_error("Unable to create a new unencrypted BIO object.\n", stdout);
																return NULL;
								}

								/* Verify successful connection */
								if (BIO_do_connect(bio) != 1) {

																print_ssl_error("Unable to connect unencrypted.\n", stdout);
																close_connection(bio);
																return NULL;
								}

								return bio;
}

int tcp_connect(const char *host,int port)
{

								struct hostent *hp;
								struct sockaddr_in addr;
								int sock;

								if(!(hp=gethostbyname(host)))
																printf("Couldn't resolve host");
								memset(&addr,0,sizeof(addr));
								addr.sin_addr=*(struct in_addr*)
																							hp->h_addr_list[0];
								addr.sin_family=AF_INET;
								addr.sin_port=htons(port);

								if((sock=(int)socket(AF_INET,SOCK_STREAM,
																													IPPROTO_TCP))<0)
																printf("Couldn't create socket");
								if(connect(sock,(struct sockaddr *)&addr,
																			sizeof(addr))<0)
																printf("Couldn't connect socket");

								return sock;
}

/**
 * Connect to a host using an encrypted stream
 */
BIO* connect_encrypted(char* host_and_port, char* store_path, char store_type, SSL_CTX** ctx, SSL** ssl) {

								BIO* bio = NULL;
								int r = 0;

								/* Set up the SSL pointers */
								*ctx = SSL_CTX_new(SSLv23_client_method());
								*ssl = NULL;

								/* Load the trust store from the pem location in argv[2] */
								if (store_type == 'f')
																r = SSL_CTX_load_verify_locations(*ctx, store_path, NULL);
								else
																r = SSL_CTX_load_verify_locations(*ctx, NULL, store_path);
								if (r == 0) {

																print_ssl_error_2("Unable to load the trust store from %s.\n", store_path, stdout);

																//Get a new certificate
																//Taken from http://stackoverflow.com/questions/10578716/obtaining-ssl-certificate-on-windows-using-c
																SSL_CTX *ctx1;
																SSL *ssl1;
																BIO *sbio;
																int sock;
																FILE *fp;

																const SSL_METHOD *meth=SSLv23_client_method();
																OpenSSL_add_ssl_algorithms();
																SSL_load_error_strings();
																ctx1=SSL_CTX_new(meth);

																/* Connect the TCP socket*/ //196.26.205.85:55105
																sock=tcp_connect("196.26.205.85",55105);

																/* Connect the SSL socket */
																ssl1=SSL_new(ctx1);
																sbio=BIO_new_socket(sock,BIO_NOCLOSE);
																SSL_set_bio(ssl1,sbio,sbio);

																if(SSL_connect(ssl1)<=0)
																								printf("SSL connect error");

																X509 *peer;
																peer=SSL_get_peer_certificate(ssl1);
																fp=fopen(store_path,"w");
																PEM_write_X509(fp, peer);
																fclose(fp);
																printf("Got new certificate\n");

																SSL_CTX_free(ctx1);

																close(sock);


																r = SSL_CTX_load_verify_locations(*ctx, store_path, NULL);
																if (r == 0) {
																								print_ssl_error_2("Unable to load the new trust store from %s.\n", store_path, stdout);
																								return NULL;
																}
								}
								cout << "timeout " << SSL_CTX_get_timeout (*ctx) << endl;
								/* Setting up the BIO SSL object */
								bio = BIO_new_ssl_connect(*ctx);
								BIO_get_ssl(bio, ssl);
								if (!(*ssl)) {

																print_ssl_error("Unable to allocate SSL pointer.\n", stdout);
																return NULL;
								}
								// SSL_set_mode(*ssl, SSL_MODE_AUTO_RETRY);

								/* Attempt to connect */
								BIO_set_conn_hostname(bio, host_and_port);

								/* Verify the connection opened and perform the handshake */
								if (BIO_do_connect(bio) < 1) {

																print_ssl_error_2("Unable to connect BIO.%s\n", host_and_port, stdout);
																return NULL;
								}

								if (SSL_get_verify_result(*ssl) != X509_V_OK) {

																print_ssl_error("Unable to verify connection result.\n", stdout);
								}

								return bio;
}

/**
 * Read a from a stream and handle restarts if nessecary
 */
ssize_t read_from_stream(BIO* bio, char* buffer, ssize_t length) {

								ssize_t r = -1;
								cout << "Read stream" << endl;

								while (r < 0) {

																r = BIO_read(bio, buffer, length);
																cout << "Read " << r << endl;
																if (r == 0) {

																								print_ssl_error("Reached the end of the data stream.\n", stdout);
																								break;

																} else if (r < 0) {

																								if (!BIO_should_retry(bio)) {

																																print_ssl_error("BIO_read should retry test failed.\n", stdout);
																																break;
																								}

																								/* It would be prudent to check the reason for the retry and handle
																								 * it appropriately here */
																}

								};

								return r;
}

/**
 * Write to a stream and handle restarts if nessecary
 */
int write_to_stream(BIO* bio, char* buffer, ssize_t length) {

								ssize_t r = -1;
								cout << "Write stream" << endl;

								while (r < 0) {

																r = BIO_write(bio, buffer, length);
																cout << "Write " << r << endl;
																if (r == 0) {

																								print_ssl_error("Write protocol failed or connection closed.\n", stdout);
																								break;

																}
																else if (r < 0) {

																								if (!BIO_should_retry(bio)) {

																																print_ssl_error("BIO_read should retry test failed.\n", stdout);
																																break;
																								}

																								/* It would be prudent to check the reason for the retry and handle
																								 * it appropriately here */
																}

								}

								return r;
}

std::string exec(char* cmd)
{
								FILE* pipe = popen(cmd, "r");
								if (!pipe) return "ERROR";
								char buffer[128];
								std::string result = "";
								while(!feof(pipe))
								{
																if(fgets(buffer, 128, pipe) != NULL)
																								result += buffer;
								}
								pclose(pipe);
								return result;
}

int readfileint(const char* fname)
{
								ifstream file (fname);
								string val;
								if(file.is_open())
								{
																getline(file,val);
																file.close();
																return atoi(val.c_str());
								}
								return -1;
}

/*************************************************************************
   // =======================================================================
   //
   //		Menu Class
   //
   //
   // =======================================================================
*************************************************************************/
class Menu
{
LCD lcd;       //Instantiate LCD class
Keypad keypad;      //Instantiate Keypad Class

// Strings for configuration file read and write
string wifiname,wifipass,wiid,wipass,wicode,comms,server;

int amount;
string help_menu;
int screen_update;    //Variable to trigger screen updates, prevents screen flickering, program will still work without this.
char key;        // indicates the key pressed, used for key presses
int row;
int col;
string str;      // empty str used for display of strings

//bools for amount and scan screens
bool in_amount_screen;
bool in_scan_screen;

// Transaction success flag
bool transactionState;

void (Menu::*foo)();

// Functions for configuration save and load
int saveConfig();
int loadConfig();
int sendxml();
// std::string exec(char* cmd);
// int readfileint(const char* fname);

//clock used for idle time out screen
clock_t IdleTimeStart;
double IdleTime;

//change
double HibernateTime;

// double lastActiveTime,IdleTime;
// IdleTimeStart = clock();
// IdleTime =( clock() - IdleTimeStart ) / (double) CLOCKS_PER_SEC;

// TODO check private vs Public function deffinitions for this app
public: //Methods can be declared here and defined below as neccessary
// Variables for threadings
// std::atomic<bool> cam_stop;
// std::atomic<bool> cam_pause;
// std::atomic<int> cam_running;
// string code, code2;
// char* v1;
// char** v;

Menu();     //Constructor
~Menu();    //Destructor

// Menu Screen Defined below:
void Home();
void Amount();
void Scan();
void Result();

void Settings();
void Connections();
void Servers();
void Merchant();
void Dev();
void Demo();
void wifiName();
void wifiPass();
void MerchantID();
void MerchantPass();
void Help();

void error(const char* msg);
void Idle();
void init_symbols();
void update_symbols();
void screen_input(char input);

void main_loop();


};

// Constructor
// Sets up GPIOs, Turns on LEDs and reads config file for settings saved previously
Menu::Menu(){

								// load settings from config.txt
								loadConfig();

								//Load images
								system("od -N 36864 -j 138 -t x1 -v -w384 ./bitmaps/WiGroup1_1.bmp > ./bitmaps/idle.txt");
								lcd.Init_fullBMP("./bitmaps/idle.txt", "Idle");
								system("od -N 36864 -j 138 -t x1 -v -w384 ./bitmaps/QR_1.bmp > ./bitmaps/QR.txt");
								lcd.Init_fullBMP("./bitmaps/QR.txt", "QR");
								Menu::init_symbols();
								lcd.lcd_clr_all();
								lcd.write_fullBMP ("Idle");

								// Use Config settings to connect to wifi / GSM
								std::string statW_up = exec("ip link | grep wlan");
								std::string statG_up = exec("ip link | grep ppp");

								if(comms.compare("wifi")==0)
								{
																if(statG_up.find("ppp") != std::string::npos)
																{
																								system("/home/wiApp/System/gprsdown.sh");
																}

																if(!(statW_up.find("state UP") != std::string::npos))
																{
																								system(("/home/wiApp/System/wifi.sh \""+wifiname+"\" \""+wifipass+"\"").c_str());
																}
								}
								else if(comms.compare("gprs")==0)
								{
																if(statW_up.find("state UP")!= std::string::npos)
																{
																								system("/home/wiApp/System/wifidown.sh");
																}
																if(!(statG_up.find("ppp") != std::string::npos))
																{
																								cout<<" Time Delay starting ---------* "<< std::endl;
																								usleep(10000000); //	5=5us || 5 000 = 5ms || 5 000 000 = 5s
																								cout<<" Time Delay ending -----------* "<< std::endl;
																								cout<<" INTO of gprs ----------------* "<< std::endl;
																								system("/home/wiApp/System/gprs.sh");
																								usleep(5000000); //	5=5us || 5 000 = 5ms || 5 000 000 = 5s
																								cout<<" out of gprs ----------------* "<< std::endl;
																}
								}


								help_menu = "";
								screen_update = 0;
								key=0;
								row=0;
								col=0;
								str="";
								in_amount_screen = true;
								in_scan_screen = false;
								transactionState = false;

								if(SETUP_OK!=sunxi_gpio_init())
								{
																cout << "Failed to initialize GPIO\n";
																return;
								}

								GPIO_CFG(LED_R,OUTPUT);
								GPIO_CFG(LED_G,OUTPUT);
								GPIO_CFG(LED_B,OUTPUT);
								GPIO_CFG(AUDIO_P,OUTPUT);

								GPIO_OUT(LED_R,1);
								GPIO_OUT(LED_G,1);
								GPIO_OUT(LED_B,1);
								GPIO_OUT(AUDIO_P,1);

}

// Destructor
Menu::~Menu(){

								sunxi_gpio_cleanup();
								//TODO DELTE any variables that may be lurking in memory

								// Clean up thread if it has not been cleaned up
								// if(the_thread.joinable()) the_thread.join();
}


/*************************************************************************
   // =======================================================================
   //
   //		Menu Screens Start below
   //
   //
   // =======================================================================
*************************************************************************/
void Menu::Home()
{

								if(screen_update==0)
								{
																//rest clock on home enter
																IdleTimeStart = clock();

																//Initialise str and key  to 0 and blank for this screen
																str="";
																key=0;
																transactionState = false;
																//Leds>>white
																GPIO_OUT(LED_R,1);
																GPIO_OUT(LED_G,1);
																GPIO_OUT(LED_B,1);

																//clear lcd
																lcd.lcd_clr_all();

																//TODO
																// Neaten up amount home screen UI
																// big str
																lcd.lcd_put_big_str (5,48,"HOME",0,3,0);
																Menu::update_symbols();
																lcd.lcd_put_str (11,67,"Amount # >",0,3,0);
																// lcd.lcd_put_str (11,0 ,"< * ",0,3,0);
																screen_update=1;
								}

								//Check idle time in (s)
								IdleTime =( clock() - IdleTimeStart ) / (double) CLOCKS_PER_SEC;
								// cout<<"Time idle (s): "<< IdleTime <<'\n';

								//change
								if(IdleTime >= 0.10)
								{
																foo=&Menu::Idle;
																screen_update=0;
								}



								//checks key presses
								key = keypad.check();

								if(key==1) // '*' Button
								{
																//Clear input string
																screen_update=0;
																return;
								}
								else if (key==2) // '#' Button
								{
																if (str.compare("1234567890")==0 || str.compare("00001")==0|| str.compare("0")==0)
																{
																								foo=&Menu::Settings;
																								screen_update=0;
																								return;
																}

																else
																{

																								foo=&Menu::Amount;
																								screen_update=0;
																								return;
																}
								}
								else if(key)
								{
																// Fixes "str" length to 11 long (Removes str[0] if str > 11)
																if(str.length() >=11)
																{
																								str.erase(0,1);
																}
																str+=key;
								}
								usleep(10000);
								return;
}

void Menu::Idle()
{

								if(screen_update==0)
								{
																str="";
																amount = 0;
																transactionState = false;

																//clear lcd
																lcd.lcd_clr_all();
																lcd.write_fullBMP ("Idle");
																// lcd.lcd_put_big_str (2,90,"IDLE",3,0,0);
																// cout<<"Time idle (s): "<< IdleTime <<'\n';

																GPIO_OUT(LED_R,1);
																GPIO_OUT(LED_G,0);
																GPIO_OUT(LED_B,1);
																screen_update=1;
								}

								//Check idle time in (s)
								IdleTime =( clock() - IdleTimeStart ) / (double) CLOCKS_PER_SEC;
								//change

								if(IdleTime >= 0.15)
								{
																//close simplemenu, load hibernatemenu, clear drivers etc.
																system("bash /home/wiApp/System/hibernate_load.sh");
								}


								//checks key presses
								key = keypad.check();

								if (key) // any key to return to home
								{
																foo=&Menu::Home;
																screen_update=0;
								}





}

/************************************************************************
   // =======================================================================
   //
   //		Settings and Sub-Settings Screens
   //
   //
   // =======================================================================
************************************************************************/
// TODO add saveconfig method call to safe config once updated
void Menu::Settings()
{
								//Init Settings screen
								if(screen_update==0)
								{
																str = "";
																key=0;
																lcd.lcd_clr_all();
																lcd.lcd_put_big_str (2,35,"SETTINGS",0,3,0);
																lcd.lcd_put_str (5,0,"1.Dev",0,3,0);
																lcd.lcd_put_str (6,0,"2.Connections",0,3,0);
																lcd.lcd_put_str (7,0,"3.Servers",0,3,0);
																lcd.lcd_put_str (8,0,"4.Merchant",0,3,0);
																lcd.lcd_put_str (9,0,"5.Volume (Not yet added)",0,3,0);
																Menu::update_symbols();
																lcd.lcd_put_str (11,0,"< * Home",0,3,0);
																//screen.home();
																screen_update=1;
								}

								key=0;
								key = keypad.check();

								if(key==1) // '*' Button
								{
																foo=&Menu::Home;
																screen_update=0;
								}
								else if (key=='1') // '1' Button
								{
																foo=&Menu::Dev;
																screen_update=0;
								}
								else if (key=='2') // '2' Button
								{
																foo=&Menu::Connections;
																screen_update=0;
								}
								else if (key=='3') // '3' Button
								{
																foo=&Menu::Servers;
																screen_update=0;
								}
								else if (key=='4') // '4' Button
								{
																foo=&Menu::Merchant;
																screen_update=0;
								}
								usleep(10000);
								return;
}


void Menu::Dev()
{
								key=0;
								key = keypad.check();
								if(screen_update==0)
								{
																str = "";
																key=0;
																lcd.lcd_clr_all();
																lcd.lcd_put_big_str (2,35,"Dev-Power",0,3,0);
																lcd.lcd_put_str (5,0,"1. ShutDown Device",0,3,0);
																lcd.lcd_put_str (6,0,"2. Restart Device",0,3,0);
																lcd.lcd_put_str (7,0,"3. Restart Program",0,3,0);
																lcd.lcd_put_str (8,0,"4. Kill Program",0,3,0);
																lcd.lcd_put_str (9,0,"5. Demo",0,3,0);
																Menu::update_symbols();
																lcd.lcd_put_str (11,0,"< * Sett.",0,3,0);
																//screen.home();
																screen_update=1;
								}
								if(key==1) // '*' Button
								{
																foo=&Menu::Settings;
																screen_update=0;

								}
								else if (key=='1') // '1' Button
								{
																lcd.write_fullBMP ("Idle");
																system("shutdown -h -P now");
								}
								else if (key=='2') // '2' Button
								{
																lcd.write_fullBMP ("Idle");
																system("reboot");
								}
								else if (key=='3') // '3' Button
								{
																//TODO relative path or change
																system("bash /home/wiApp/System/menu_restart.sh");
																// system("manmakemachine2014");
								}
								else if (key=='4') // '4' Button
								{
																lcd.lcd_clr_all();
																lcd.lcd_put_big_str (3,0,"Program Killed.",0,3,0);
																lcd.lcd_put_big_str (7,0,"I am now dead..",0,3,0);
																lcd.lcd_put_str (11,0,"Bleh...",0,3,0);
																exit(0);
								}
								else if(key=='5')
								{
																foo=&Menu::Demo;
																screen_update=0;
																return;
								}
								usleep(10000);
								return;
}
void Menu::Demo()
{
								key=0;
								key = keypad.check();
								if(screen_update==0)
								{
																str = "";
																key=0;
																lcd.lcd_clr_all();
																lcd.lcd_put_big_str (2,48,"Demo",0,3,0);
																lcd.lcd_put_str (5,0,"1. Show QR Code",0,3,0);
																lcd.lcd_put_str (6,0,"2. Record Sound <7s>",0,3,0);
																lcd.lcd_put_str (7,0,"3. Play Recording",0,3,0);
																Menu::update_symbols();
																lcd.lcd_put_str (11,0,"< * Dev.",0,3,0);
																//screen.home();
																screen_update=1;
								}
								if(key==1) // '*' Button
								{
																foo=&Menu::Dev;
																screen_update=0;

								}
								else if (key=='1') // '1' Button
								{
																lcd.write_fullBMP ("QR");
																usleep(8500000);
																screen_update=0;
								}
								else if (key=='2') // '2' Button
								{
																// use "alsamixer -V capture in ssh" to setup volumes before use
																system("arecord -f dat -r 60000 -D hw:0,0 -d 7 test.wav");
								}
								else if (key=='3') // '3' Button
								{
																// use "alsamixer -V playback in ssh" to setup volumes before use
																system("aplay -D hw:0,0 test.wav");
								}
								usleep(10000);
								return;
}

// Sets what connections are used wifi / gprs
void Menu::Connections()
{
								if(screen_update==0)
								{
																str = "";
																key=0;
																lcd.lcd_clr_all();
																// TODO Menu screen options for WIFI / GPRS
																// 1. Wifi
																// 2. GSM
																// 3. Wifi Settings
																lcd.lcd_put_big_str (2,35,"SETT_CON",0,3,0);
																lcd.lcd_put_str (5,0,"1.Wifi",0,3,0);
																lcd.lcd_put_str (6,0,"2.GSM",0,3,0);
																Menu::update_symbols();
																lcd.lcd_put_str (11,0,"< * Sett.",0,3,0);
																lcd.lcd_put_str (11,79,"Home # >",0,3,0);
																//screen.home();
																screen_update=1;
								}
								while(true)
								{
																key = keypad.check();

																if(key==1) // '*' Button
																{
																								foo=&Menu::Settings;
																								screen_update=0;
																								return;
																}
																else if (key==2) // '#' Button
																{
																								foo=&Menu::Home;
																								screen_update=0;
																								return;
																}

																else if(key=='1')
																{
																								std::string stat = exec("ip link | grep ppp");
																								if (stat.find("ppp") != std::string::npos)
																								{
																																system("/home/wiApp/System/gprsdown.sh");
																								}

																								comms="wifi";

																								foo=&Menu::wifiName;
																								screen_update=0;
																								return;
																}

																else if(key=='2')
																{
																								//TODO clean up this display
																								// lcd.lcd_put_big_str (A,B,C,X,Y,Z);
																								// A: Page - 96px/8 {0 --> 11}
																								// B: Number of px from left
																								// C: Takes in String or char[]
																								// X: Grey scale, background 0,1,2,3 (Dark to white)
																								// Y: Grey scale, for font same as above
																								// Z: Flipping 0/1
																								lcd.lcd_clr_all();
																								lcd.lcd_put_big_str (2,35,"Connecting to server:",0,3,0);


																								std::string statDown = exec("ip link | grep wlan");
																								if (statDown.find("state UP")!= std::string::npos)
																								{
																																system("/home/wiApp/System/wifidown.sh");
																								}

																								comms="gprs";
																								saveConfig();
																								lcd.lcd_clr_all();
																								lcd.lcd_put_str (6,0,"Connecting to GSM...");
																								system("/home/wiApp/System/gprs.sh");
																								usleep(7000000); //	5=5us || 5 000 = 5ms || 5 000 000 = 5s

																								foo=&Menu::Home;
																								screen_update=0;
																								return;
																}

								}

								usleep(10000);
								return;
}

void Menu::wifiName()
{
								if(screen_update==0)
								{
																str=wifiname;
																key=0;
																row=0;
																col=0;

																//TODO Neaten up display
																lcd.lcd_clr_all();
																lcd.lcd_put_big_str (3,25,"Enter SSID:");
																lcd.lcd_put_str (6,0,str);
																lcd.lcd_put_str (7,((str.length()-1)*6),"^");
																Menu::update_symbols();
																lcd.lcd_put_str (10, 3,"-Press '1' for Help-",0,3,0);
																lcd.lcd_put_str (11,0,"< * Conn.",0,3,0);
																lcd.lcd_put_str (11,79,"Save # >",0,3,0);
																screen_update=1;
								}
								while(true)
								{
																//Enables text entry
																key=keypad.check();
																if(key==1) // * to clear
																{
																								if(str.length()==0)
																								{
																																foo=&Menu::Connections;
																																screen_update=0;
																																return;
																								}
																								else
																								{
																																lcd.lcd_clr_area(7, ((str.length()-1)*6), 1, 1);
																																lcd.lcd_clr_area(6, 0, 1, (str.length()));
																																str="";
																								}
																}
																else if(key==2)
																{
																								wifiname=str;
																								foo=&Menu::wifiPass;
																								screen_update=0;
																								return;
																}
																else if(key=='1')
																{
																								foo=&Menu::Help;
																								help_menu = "wifiName";
																								screen_update=0;
																								return;
																}
																else if(key)
																{
																								Menu::screen_input(key);
																}
																usleep(10000);
								}
								return;
}

void Menu::wifiPass()
{
								if(screen_update==0)
								{
																str=wifipass;
																key=0;
																row=0;
																col=0;

																//TODO Neaten up display
																lcd.lcd_clr_all();
																lcd.lcd_put_big_str (3,10,"Enter Password:");
																lcd.lcd_put_str (6,0,str);
																lcd.lcd_put_str (7,((str.length()-1)*6),"^");
																Menu::update_symbols();
																lcd.lcd_put_str (10, 3,"-Press '1' for Help-",0,3,0);
																lcd.lcd_put_str (11,0,"< * Conn.",0,3,0);
																lcd.lcd_put_str (11,79,"Save # >",0,3,0);
																screen_update=1;
								}
								while(true)
								{
																//Enables text entry
																key=keypad.check();
																if(key==1) // * to clear
																{
																								if(str.length()==0)
																								{
																																foo=&Menu::wifiName;
																																screen_update=0;
																																return;
																								}
																								else
																								{
																																lcd.lcd_clr_area(7, ((str.length()-1)*6), 1, 1);
																																lcd.lcd_clr_area(6, 0, 1, (str.length()));
																																str="";
																								}
																}
																else if(key==2)
																{
																								wifipass=str;
																								saveConfig();

																								lcd.lcd_clr_all();
																								lcd.lcd_put_str (6,0,"Connecting to WiFi...");

																								system(("/home/wiApp/System/wifi.sh \""+wifiname+"\" \""+wifipass+"\"").c_str());
																								foo=&Menu::Home;
																								screen_update=0;
																								return;
																}
																else if(key=='1')
																{
																								foo=&Menu::Help;
																								help_menu = "wifiPass";
																								screen_update=0;
																								return;
																}
																else if(key)
																{
																								Menu::screen_input(key);
																}
																usleep(10000);
								}
								return;
}

//Sets which servers the devcie will connect too to send wiCodes
void Menu::Servers()
{
								if(screen_update==0)
								{
																str = "";
																key=0;

																//TODO Neaten up graphics
																lcd.lcd_clr_all();
																lcd.lcd_put_big_str (2,0,"SETT_SERVER");
																lcd.lcd_put_str (5,0,"1. PROD 1");
																lcd.lcd_put_str (6,0,"2. PROD 2");
																lcd.lcd_put_str (7,0,"3. QA");
																Menu::update_symbols();
																lcd.lcd_put_str (11,0,"< * Sett.",0,3,0);
																//screen.home();
																screen_update=1;
								}


								while (true)
								{
																key = keypad.check();

																if(key==1) // '*' Button
																{
																								foo=&Menu::Settings;
																								screen_update=0;
																								return;
																}
																else if (key==2) // '#' Button
																{
																								foo=&Menu::Home;
																								screen_update=0;
																								return;
																}
																else if(key=='3')
																{
																								server="qa";
																								saveConfig();
																								foo=&Menu::Home;
																								screen_update=0;
																								return;
																}
																else if(key=='1')
																{
																								//TODO uncomment FOR PRODUCTION
																								server="production1";
																								// server="qa";
																								saveConfig();
																								foo=&Menu::Home;
																								screen_update=0;
																								return;
																}
																else if(key=='2')
																{
																								//TODO uncomment FOR PRODUCTION
																								server="production2";
																								// server="qa";
																								saveConfig();
																								foo=&Menu::Home;
																								screen_update=0;
																								return;
																}

								}

								usleep(10000);

								return;
}

//Sets the merchant ID and passwords
void Menu::Merchant()
{
								if(screen_update==0)
								{
																str = "";
																key=0;
																lcd.lcd_clr_all();
																lcd.lcd_put_big_str (2,0,"SETT_MERCHANT");
																lcd.lcd_put_str (5,0,"1. MerchID");
																lcd.lcd_put_str (6,0,"2. MerchPass");
																Menu::update_symbols();
																lcd.lcd_put_str (11,0,"< * Sett.",0,3,0);
																//screen.home();
																screen_update=1;
								}

								key = keypad.check();

								if(key==1) // '*' Button
								{
																foo=&Menu::Settings;
																screen_update=0;
																return;
								}
								else if (key==2) // '#' Button
								{
																foo=&Menu::Home;
																screen_update=0;
																return;
								}
								else if(key=='1')
								{

																foo=&Menu::MerchantID;
																screen_update=0;
																return;
								}
								else if(key=='2')
								{

																foo=&Menu::MerchantPass;
																screen_update=0;
																return;
								}

								usleep(10000);
								return;
}
//Sets the merchant ID
void Menu::MerchantID()
{
								if(screen_update==0)
								{
																str=wiid;
																key=0;
																row=0;
																col=0;

																//TODO Neaten up display
																lcd.lcd_clr_all();
																lcd.lcd_put_big_str (3,20,"MERCHANT ID");
																lcd.lcd_put_str (6,0,str);
																lcd.lcd_put_str (7,((str.length()-1)*6),"^");
																Menu::update_symbols();
																lcd.lcd_put_str (10,12,"-Press '1' for Help-");
																lcd.lcd_put_str (11,0,"< * Mer.Set.");
																lcd.lcd_put_str (11,79,"Save # >");
																screen_update=1;
								}
								while(true)
								{
																//Enables text entry
																key=keypad.check();
																if(key==1) // * to clear
																{
																								if(str.length()==0)
																								{
																																foo=&Menu::Merchant;
																																screen_update=0;
																																return;
																								}
																								else
																								{
																																lcd.lcd_clr_area(7, ((str.length()-1)*6), 1, 1);
																																lcd.lcd_clr_area(6, 0, 1, (str.length()));
																																str="";
																								}
																}
																else if(key==2)
																{
																								wiid=str;
																								foo=&Menu::MerchantPass;
																								screen_update=0;
																								return;
																}
																else if(key=='1')
																{
																								foo=&Menu::Help;
																								help_menu = "MerchantID";
																								screen_update=0;
																								return;
																}
																else if(key)
																{
																								Menu::screen_input(key);
																}
																usleep(10000);
								}
								return;
}
//Sets the merchant password
void Menu::MerchantPass()
{
								if(screen_update==0)
								{
																str=wipass;
																key=0;
																row=0;
																col=0;

																//TODO Neaten up display
																lcd.lcd_clr_all();
																lcd.lcd_put_big_str (3,4,"MERCH. PASSWORD");
																lcd.lcd_put_str (6,0,str);
																lcd.lcd_put_str (7,((str.length()-1)*6),"^");
																Menu::update_symbols();
																lcd.lcd_put_str (10,12,"-Press '1' for Help-");
																lcd.lcd_put_str (11,0,"< * Mer. ID");
																lcd.lcd_put_str (11,79,"Save # >");
																screen_update=1;
								}
								while(true)
								{
																//Enables text entry
																key=keypad.check();
																if(key==1) // * to clear
																{
																								if(str.length()==0)
																								{
																																foo=&Menu::MerchantID;
																																screen_update=0;
																																return;
																								}
																								else
																								{
																																lcd.lcd_clr_area(7, ((str.length()-1)*6), 1, 1);
																																lcd.lcd_clr_area(6, 0, 1, (str.length()));
																																str="";
																								}
																}
																else if(key==2)
																{
																								wipass=str;
																								foo=&Menu::Home;
																								saveConfig();
																								screen_update=0;
																								return;
																}
																else if(key=='1')
																{
																								foo=&Menu::Help;
																								help_menu = "MerchantPass";
																								screen_update=0;
																								return;
																}
																else if(key)
																{
																								Menu::screen_input(key);
																}
																usleep(10000);
								}
								return;
}
/*************************************************************************
   // =======================================================================
   //
   //		Transaction screens
   //
   //
   // =======================================================================
*************************************************************************/
// Enter amount screen:
// Start threading (Boot camera)
// Pause "Qr processing"
// Take in Amount
// Display Amount on Screen
// When Aount is ented AND camera is ready to scan turn Blue
// Update screen to indicate to user to present QR code
// unpause QR Processing - *pause
// Recieve scanned QR from thread
void Menu::Amount()
{
								transactionState = false;
								char * amountStr;

								string code = ""; // used for qr codes entered by keypad
								string code2 = ""; // used by qr codes scanned by camera

								char* v1="WiReader";
								char** v=&v1;

								atomic<bool> cam_stop (false);
								atomic<int> cam_running (0);
								atomic<bool> cam_pause (true);

								//sets cam_stop to false so the camera thread can boot up
								cam_stop = false;
								//sets pause so the camera does not scan for images until required
								cam_pause = true;

								//runs the camera thread
								thread the_thread(qrscan,1, v, &code2, &cam_stop, &cam_running, &cam_pause);
								in_amount_screen = true;
								in_scan_screen = false;
								screen_update=0;
								std::string str2 = "";
								// Loops inside member function to avoid returning through main and aborting the thread process (qrscan) which is locked into this member
								while(true)
								{
																key = keypad.check();

																// Entered into "amount screen"
																if (in_amount_screen)
																{

																								// Amount Screen init:
																								if (screen_update==0)
																								{
																																str ="";
																																key=0;
																																code = "";
																																code2 ="";
																																//NOTE can init this outside the while true to redisplay the amount
																																amount=0;

																																//uses double bools could be improved
																																in_amount_screen = true;
																																in_scan_screen = false;


																																GPIO_OUT(LED_R,1);
																																GPIO_OUT(LED_G,1);
																																GPIO_OUT(LED_B,1);

																																// TODO
																																// Neaten up amount screen UI
																																lcd.lcd_clr_all();
																																lcd.lcd_put_big_str (5,15,"ENTER AMOUNT",0,3,0);
																																Menu::update_symbols();
																																lcd.lcd_put_str (11,79,"Scan # >",0,3,0);
																																lcd.lcd_put_str (11,0,"< * Home",0,3,0);

																																screen_update=1;

																								}

																								//if no string and * pressed then go back a screen to Home
																								if(key==1 && str == "") // '*' Button
																								{
																																//stop camera thread before leaving menu screen
																																cam_stop=true;
																																//TODO Clear input string
																																foo=&Menu::Home;
																																screen_update=0;
																																// join camera thread before leaving amount screen
																																the_thread.join();
																																break;
																								}
																								else if(key ==1 && str.length()>=1)
																								{
																																// str.erase(0,1);
																																lcd.lcd_clr_area(7,80-((str.length())-4)*8,2,str.length());
																																str ="";
																																amount = 0;
																																lcd.lcd_put_str (11,0,"< * Home ",0,3,0);
																								}

																								// if # pressed set scan flag true move to scan menu
																								else if(key == 2)
																								{
																																str ="";
																																in_amount_screen = false;
																																in_scan_screen = true;
																																screen_update=0;
																								}

																								else if(key)
																								{
																																amount=amount*10+key-'0';
																																if (amount>100000000) amount=100000000; //To prevent int overflow, limit amount to 1 million rand
																																cout<<"Amount Entered1: "<< amount <<'\n'; // DEBUG out to console
																																str2 = std::to_string(amount);
																																if(str2.length()>2)
																																								str = "R " + str2.substr(0,str2.length()-2) + "." + str2.substr(str2.length()-2,2);
																																if(str2.length()==2)
																																								str = "R 0." + str2;
																																if(str2.length()==1)
																																								str = "R 0.0" + str2;
																																cout<<"Amount Entered2: "<< str <<'\n'; // DEBUG out to console
																																lcd.lcd_put_big_str (7,80-((str.length())-4)*8,str);
																																cout<<"String Entered: "<< str <<'\n';
																																lcd.lcd_put_str (11,0,"< * Clear",0,3,0);
																								}
																								key=0;
																}
																/*********************************************************
																   Enters in to sub screen scan screen
																*********************************************************/
																else if (in_scan_screen)
																{

																								if (screen_update==0)
																								{
																																str ="";
																																//clears the code used for entering of QR codes in this menu
																																code = "";
																																//TODO
																																// Neaten up amount screen UI
																																lcd.lcd_clr_all();
																																lcd.lcd_put_big_str (3,0,"SCAN OR ENTER QR",0,3,0);
																																Menu::update_symbols();
																																lcd.lcd_put_str (11,79,"Send # >",0,3,0);
																																lcd.lcd_put_str (11,0,"< * Amount",0,3,0);
																																//TODO insert amount at SCAN SCREEN

																																screen_update=1;
																								}


																								// QR has been found, store as code2 >> wicode and break out to Result screen
																								if (cam_stop)
																								{
																																// Goes white while connecting (Connection done in results method)
																																GPIO_OUT(LED_R,1);
																																GPIO_OUT(LED_G,1);
																																GPIO_OUT(LED_B,1);
																																// popen("aplay -D hw:0,0 /home/olimex/beep.wav","r");
																																//TODO look at threading the sound so it plays faster
																																// BEEP
																																system("aplay -D hw:0,0 /home/wiApp/System/beep.wav");

																																cout << "Scanned: " << code2 << endl;
																																wicode=code2;

																																the_thread.join();
																																in_amount_screen = true;
																																in_scan_screen = false;
																																foo=&Menu::Result;
																																screen_update=0;

																																break;
																								}
																								// waits for the camera to have booted up before allowing scan mode to continue
																								else if (cam_running == 1)
																								{
																																GPIO_OUT(LED_R,0);
																																GPIO_OUT(LED_G,0);
																																GPIO_OUT(LED_B,1);
																																// if the camera is running and in scan screen, ready for QR scan
																																cam_pause = false;
																																cam_running == 0;
																								}

																								//if no string and * pressed then go back a screen to Amount screen
																								if(key==1 && str == "") // '*' Button
																								{
																																in_amount_screen = true;
																																in_scan_screen = false;
																																cam_pause = true;
																																screen_update=0;

																								}
																								else if(key==1 && str.length()>0) // '*' Button and str > 0
																								{
																																lcd.lcd_clr_area(5,0,2,str.length());
																																//TODO remove the next line of test code (These do the same thing but the one below has more functions 0,1,2,3 as final variable for grey scale)
																																lcd.lcd_clr_area(5,0,2,str.length(),0);
																																str = "";
																																//clear entered wicode
																																code = "";
																																//TODO clear Code off LCD screen
																								}
																								// if # pressed set scan flag true move to transaction menu
																								else if(key == 2 && (code.length() >= 1))
																								{
																																// Goes white while connecting (Connection done in results method)
																																GPIO_OUT(LED_R,1);
																																GPIO_OUT(LED_G,1);
																																GPIO_OUT(LED_B,1);

																																cam_stop=true;
																																str ="";
																																in_amount_screen = true;
																																in_scan_screen = false;
																																the_thread.join();
																																wicode = code;

																																foo=&Menu::Result;
																																screen_update=0;

																																return;
																								}
																								else if(key == 2 && (code == ""))
																								{
																																cam_stop=true;
																																str ="";
																																in_amount_screen = true;
																																in_scan_screen = false;
																																the_thread.join();
																																wicode = "Empty";

																																foo=&Menu::Result;
																																screen_update=0;

																																return;
																								}
																								else if(key)
																								{
																																//TODO add display output! / update amount screen
																																if(str.length() >=16)
																																{
																																								str.erase(0,1);
																																}
																																str+=key;
																																code = str;
																																//TODO display QR entered on Code on screen
																																lcd.lcd_put_big_str (5,0,code,0,3,0);

																																// cout<<"QR CODE Manually Entered: "<< code <<'\n'; // DEBUG out to console
																								}
																}

																usleep(10000);
								}

								return;
}

// Display results of transaction screen
void Menu::Result()
{


								if(screen_update==0)
								{
																//NOTE if you redirect here using the menu pointer *foo you will have a bad day
																// The send XML will be called again. DONOT DO THIS

																// TODO neaten up display of connection connecting screen
																//Display Connecting
																// lcd.lcd_put_big_str (A,B,C,X,Y,Z);
																// A: Page - 96px/8 {0 --> 11}
																// B: Number of px from left
																// C: Takes in String or char[]
																// X: Grey scale, background 0,1,2,3 (Dark to white)
																// Y: Grey scale, for font same as above
																// Z: Flipping 0/1
																lcd.lcd_clr_all();
																lcd.lcd_put_str (2,0,"Connecting to server:",0,3,0);
																lcd.lcd_put_big_str (4,35,server,0,3,0);
																lcd.lcd_put_big_str (6,35,"",0,3,0);

																// sends wicode via xml packet and settings configured
																// TODO green/red lights on transact fail/pass  (IN send XML)
																// TODO write errorrs or success to screen
																transactionState = false;
																sendxml();

																str = "";
																key=0;

																//Display results of transaction
																//TODO neaten up display screen here
																// TODO Display QR code + Amount
																lcd.lcd_clr_all();
																lcd.lcd_put_big_str (3,35,"RESULT",0,3,0);
																if (transactionState)
																{
																								lcd.lcd_put_big_str (5,35,"SUCCESS!",0,3,0);
																}
																else if (!transactionState)
																{
																								lcd.lcd_put_big_str (5,35,"FAILURE!",0,3,0);
																}
																else
																{
																								lcd.lcd_put_big_str (5,35,"Error!",0,3,0);
																}
																lcd.lcd_put_big_str (7,0,"QR:",0,3,0);
																lcd.lcd_put_big_str (9,0,wicode,0,3,0);

																Menu::update_symbols();
																lcd.lcd_put_str (11,0,"< * Home",0,3,0);
																lcd.lcd_put_str (11,79,"Home # >",0,3,0);
																//screen.home();
																screen_update=1;


								}


								key = keypad.check();
								if(key==1) // '*' Button
								{
																foo=&Menu::Home;
																screen_update=0;
								}
								else if (key==2) // '#' Button
								{
																foo=&Menu::Home;
																screen_update=0;
								}
								key=0;
								usleep(10000);
								return;
}
// Display results of transaction screen
void Menu::Help()
{
								if(screen_update==0)
								{
																lcd.lcd_clr_all();
																if(help_menu=="wifiName")
																								lcd.lcd_put_str (0,4,"INFO - WifiName");
																if(help_menu=="wifiPass")
																								lcd.lcd_put_str (0,4,"INFO - WifiName");
																if(help_menu=="MerchantID")
																								lcd.lcd_put_str (0,4,"INFO - Merch_ID");
																if(help_menu=="MerchantPass")
																								lcd.lcd_put_str (0,4,"INFO-Merch_Pass");
																lcd.lcd_put_str (1,0,"- - - - - - - - - - -");
																lcd.lcd_put_str (2,0," 2 = Up & 8 = Down");
																lcd.lcd_put_str (3,0,"Use 2&8 to go between");
																lcd.lcd_put_str (4,0,"1Lowercase 2Uppercase");
																lcd.lcd_put_str (5,0,"3.Numbers &4.Symbols ");
																lcd.lcd_put_str (6,0,"- - - - - - - - - - -");
																lcd.lcd_put_str (7,0,"Use 4&6 to go between");
																lcd.lcd_put_str (8,0,"indiv. letter/symbols");
																lcd.lcd_put_str (9,0,"5 = Select 0 = Delete");
																lcd.lcd_put_str (10,0,"- - - - - - - - - - -");
																lcd.lcd_put_str (11,67,"Return # >");
																lcd.lcd_put_str (11,0,"< * Home");
																screen_update=1;
								}
								if(help_menu=="" && screen_update==0)
								{
																lcd.lcd_put_str (0,4,"INFO - Error");
																lcd.lcd_put_str (1,0,"- - - - - - - - - - -");
																lcd.lcd_put_str (10,0,"- - - - - - - - - - -");
																lcd.lcd_put_str (11,67,"Return # >");
																lcd.lcd_put_str (11,0,"< * Home");
								}


								key = keypad.check();
								if(key==1) // '*' Button
								{
																foo=&Menu::Home;
																screen_update=0;
								}
								else if (key==2) // '#' Button
								{
																if(help_menu=="wifiName") foo=&Menu::wifiName;
																else if(help_menu=="wifiPass") foo=&Menu::wifiPass;
																else if(help_menu=="MerchantID") foo=&Menu::MerchantID;
																else if(help_menu=="MerchantPass") foo=&Menu::MerchantPass;
																else if(help_menu=="") foo=&Menu::Home;

																screen_update=0;
								}
								key=0;
								usleep(10000);
								return;
}

//---------------------------------------------------------------------------------------------
/*
   WTF managment

   Menu:error
 */
//---------------------------------------------------------------------------------------------
void Menu::error(const char *msg)
{
								perror(msg);
								//lcd.lcd_put_str (0,0,msg);
								return;
}

//---------------------------------------------------------------------------------------------
/*
   Configuration managment

   saveConfig
   loadConfig
 */
//---------------------------------------------------------------------------------------------
int Menu::saveConfig()
{

								ofstream savefile ("config.txt");

								if(savefile.is_open())
								{
																savefile << wiid << endl;
																savefile << wipass << endl;
																savefile << wifiname << endl;
																savefile << wifipass << endl;
																savefile << comms << endl;
																savefile << server << endl;
																savefile.close();
								}
								else return -1;
								return 0;
}

int Menu::loadConfig()
{
								ifstream savefile ("config.txt");

								if(savefile.is_open()) {
																getline(savefile,wiid);
																getline(savefile,wipass);
																getline(savefile,wifiname);
																getline(savefile,wifipass);
																getline(savefile,comms);
																getline(savefile,server);
																savefile.close();
								}

								else
								{
																wiid="";
																wipass="";
																wifiname="";
																wifipass="";
																comms="";
																server="";
																return -1;
								}
								return 0;
}
//---------------------------------------------------------------------------------------------
/*
   send XML via SSL

   saveConfig
   loadConfig
 */
//---------------------------------------------------------------------------------------------
int Menu::sendxml()
{

								if(server.compare("qa")==0)
								{
																cout << "qa" << endl;
																int sockfd, portno, n;
																struct sockaddr_in serv_addr;
																struct hostent *server;
																//lcd.lcd_put_str (0,0,"Start Transaction");

																//TODO FIX BUFFER LENGTH - check it does not mess with SSL
																char buffer[512];

																sprintf(buffer,"<wipos ver=\"2.0\" wid=\"%s\" wipass=\"%s\" tellerid=\"merchantname_01\"><transrx><cusid>%s</cusid><amnt>%d</amnt><merchref>%s</merchref></transrx></wipos>",wiid.c_str(),wipass.c_str(),wicode.c_str(),amount,wiid.c_str());
																cout << "Sending: " << buffer << endl;

																portno = 41001;
																//lcd.lcd_put_str (0,0,"Start Connection");
																sockfd = socket(AF_INET, SOCK_STREAM, 0);

																if (sockfd < 0)
																{
																								error("ERROR opening socket");
																								return 1;
																}
																server = gethostbyname("196.26.205.86");

																if (server == NULL)
																{
																								fprintf(stderr,"ERROR, no such host\n");
																								//lcd.lcd_put_str (0,0,"ERROR, no such host");
																								close(sockfd);
																								return 1;
																}

																bzero((char *) &serv_addr, sizeof(serv_addr));
																serv_addr.sin_family = AF_INET;
																bcopy((char *)server->h_addr,
																						(char *)&serv_addr.sin_addr.s_addr,
																						server->h_length);
																serv_addr.sin_port = htons(portno);

																if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
																{
																								error("ERROR connecting");
																								close(sockfd);
																								return 1;
																}
																//bzero(buffer,512);
																// fgets(buffer,511,stdin);
																//buffer="<wipos ver=\"2.0\" wid=\"1050\" wipass=\"test\" tellerid=\"merchantname_01\"><transrx><cusid>7052278</cusid><amnt>5056</amnt><merchref>1050</merchref></transrx></wipos>";
																//lcd.lcd_put_str (0,0,"Sending Request ");
																n = write(sockfd,buffer,strlen(buffer));

																if (n < 0)
																{
																								error("ERROR writing to socket");
																								close(sockfd);
																								return 1;
																}

																bzero(buffer,512);

																//lcd.lcd_put_str (0,0,"Waiting Response");
																n = read(sockfd,buffer,511);

																if (n < 0)
																{
																								error("ERROR reading from socket");
																								close(sockfd);
																								return 1;
																} //Store not found

																//    printf("%s\n",buffer);
																//    char* a = strstr(buffer,"<desc>");
																//    char* b = strstr(buffer,"</desc>");
																//    b[0]=0;
																// //lcd.lcd_clr_all();

																// cout << "******************************" << &a[6] << "*******************************" << endl;

																//buffer="<wipos ver=\"2.0\" wid=\"1050\" wipass=\"test\" tellerid=\"merchantname_01\"><transrx><cusid>7052278</cusid><amnt>5056</amnt><merchref>1050</merchref></transrx></wipos>";
																printf("%s\n",buffer);

																// char* errA;
																// char* errB;
																// errA = strstr(buffer,"<ercode>");
																// errB = strstr(buffer,"</ercode>");
																// errB[0]=0;
																std::string strBuffer = buffer;
																char errA = strBuffer.find("<ercode>");
																char errB = strBuffer.find("</ercode>");

																std::string strErr     = strBuffer.substr(errA+8,errB-(errA+8));

																cout << strErr << endl;
																cout << "******************" << strErr << "******************" << endl;
																int strint = std::stoi (strErr,nullptr,10);
																cout << "******************" << strint << "******************" << endl;

																// strErr >> ia;
																// cout << "--------int---------" << ia << "--------int-------" << endl;
																if(strint == -1)
																{
																								GPIO_OUT(LED_R,0);
																								GPIO_OUT(LED_G,1);
																								GPIO_OUT(LED_B,0);
																								transactionState = true;
																}
																else
																{
																								GPIO_OUT(LED_R,1);
																								GPIO_OUT(LED_G,0);
																								GPIO_OUT(LED_B,0);
																								transactionState = false;
																}
																// char * out = new char [strErr.length()+1];
																//  std::strcpy (out, strErr.c_str());
																//  cout << out << endl;


																// char derpderp[] = &errA[8];

																// derpderp = errA;
																// int herpherp;
																// derpderp = &errA[8];
																// strcpy(derpderp, &errA[8]);
																// cout << "----derpderp----------" << errA << "--------derpderp-----" << endl;
																// cout << "----derpderp----------" << DERP << "--------derpderp-----" << endl;
																// std::stringstream derpstr(derpderp);
																// derpderp >> herpherp;

																// cout << "------------------" << &errB[1] << "----------------" << endl;
																//  char tempC[100];
																// strcpy(tempC,&errA[8]);
																// int ia = tempC - '0';
																// // "<ercode>000</ercode>"
																// cout << "******************************" << &errA[8] << "*******************************" << endl;
																printf("%s\n",buffer);
																// cout << "------------------------------" << ia << "------------------------------" << endl;


																// lcd.lcd_put_str (0,0,&errA[6]);
																close(sockfd);

																return 0;
								}

								else if(server.compare("production1")==0)
								{
																cout << "production1" << endl;

																// //SSL
																// //==========================================
																//lcd.lcd_put_str (0,0,"Start Transaction");

																char* host_and_port = "196.26.205.85:55105"; /* localhost:4422 */
																// char* server_request = argv[2]; /* "GET / \r\n\r\n" */
																char server_request[512];
																sprintf(server_request,"<wipos ver=\"2.0\" wid=\"%s\" wipass=\"%s\" tellerid=\"merchantname_01\"><transrx><cusid>%s</cusid><amnt>%d</amnt><merchref>%s</merchref></transrx></wipos>",wiid.c_str(),wipass.c_str(),wicode.c_str(),amount,wiid.c_str());
																cout << "Sending: " << server_request << endl;
																char* store_path ="./certificate.pem"; /* /home/user/projects/sslclient/certificate.pem */
																char store_type = 'f'; /* f = file, anything else is a directory structure */
																char connection_type = 'e'; /* e = encrypted, anything else is unencrypted */

																char buffer[4096];
																buffer[0] = 0;

																BIO* bio;
																SSL_CTX* ctx = NULL;
																SSL* ssl = NULL;

																/* initilise the OpenSSL library */
																init_openssl();
																// cout << "SSL Version: " << SSLeay_version(SSLEAY_VERSION) << endl;

																//lcd.lcd_put_str (0,0,"Start Connection");
																/* encrypted link */
																if (connection_type == 'e')
																{

																								if ((bio = connect_encrypted(host_and_port, store_path, store_type, &ctx, &ssl)) == NULL)
																								{
																																//lcd.lcd_put_str (0,0,"Connection failed");
																																return 1;
																								}
																}
																/* unencrypted link */
																else if ((bio = connect_unencrypted(host_and_port)) == NULL)
																								return 1;

																//lcd.lcd_put_str (0,0,"Sending Request ");
																write_to_stream(bio, server_request, strlen(server_request));
																//lcd.lcd_put_str (0,0,"Waiting Response");
																read_from_stream(bio, buffer, 4096);

																printf("%s\r\n", buffer);
																std::string strBuffer = buffer;
																char errA = strBuffer.find("<ercode>");
																char errB = strBuffer.find("</ercode>");

																std::string strErr     = strBuffer.substr(errA+8,errB-(errA+8));

																cout << strErr << endl;
																cout << "******************" << strErr << "******************" << endl;
																int strint = std::stoi (strErr,nullptr,10);
																cout << "******************" << strint << "******************" << endl;

																// strErr >> ia;
																// cout << "--------int---------" << ia << "--------int-------" << endl;
																if(strint == -1)
																{
																								GPIO_OUT(LED_R,0);
																								GPIO_OUT(LED_G,1);
																								GPIO_OUT(LED_B,0);
																								transactionState = true;
																}
																else
																{
																								GPIO_OUT(LED_R,1);
																								GPIO_OUT(LED_G,0);
																								GPIO_OUT(LED_B,0);
																								transactionState = false;
																}

																if (close_connection(bio) == 0)
																								return 1;

																/* clean up the SSL context resources for the encrypted link */
																if (connection_type == 'e')
																								SSL_CTX_free(ctx);

																return 0;
								}

								else
								{
																cout << "production2" << endl;
																// //SSL
																// //==========================================
																//lcd.lcd_put_str (0,0,"Start Transaction");

																char* host_and_port = "196.26.205.85:52102"; /* localhost:4422 */
																// char* server_request = argv[2]; /* "GET / \r\n\r\n" */
																char server_request[512];
																sprintf(server_request,"<wipos ver=\"2.0\" wid=\"%s\" wipass=\"%s\" tellerid=\"merchantname_01\"><transrx><cusid>%s</cusid><amnt>%d</amnt><merchref>%s</merchref></transrx></wipos>",wiid.c_str(),wipass.c_str(),wicode.c_str(),amount,wiid.c_str());
																cout << "Sending: " << server_request << endl;
																char* store_path ="./certificate.pem"; /* /home/user/projects/sslclient/certificate.pem */
																char store_type = 'f'; /* f = file, anything else is a directory structure */
																char connection_type = 'e'; /* e = encrypted, anything else is unencrypted */

																char buffer[4096];
																buffer[0] = 0;

																BIO* bio;
																SSL_CTX* ctx = NULL;
																SSL* ssl = NULL;

																/* initilise the OpenSSL library */
																init_openssl();
																// cout << "SSL Version: " << SSLeay_version(SSLEAY_VERSION) << endl;

																//lcd.lcd_put_str (0,0,"Start Connection");
																/* encrypted link */
																if (connection_type == 'e')
																{

																								if ((bio = connect_encrypted(host_and_port, store_path, store_type, &ctx, &ssl)) == NULL)
																								{
																																//lcd.lcd_put_str (0,0,"Connection failed");
																																return 1;
																								}
																}
																/* unencrypted link */
																else if ((bio = connect_unencrypted(host_and_port)) == NULL)
																								return 1;

																//lcd.lcd_put_str (0,0,"Sending Request ");
																write_to_stream(bio, server_request, strlen(server_request));
																//lcd.lcd_put_str (0,0,"Waiting Response");
																read_from_stream(bio, buffer, 4096);

																printf("%s\r\n", buffer);

																std::string strBuffer = buffer;
																char errA = strBuffer.find("<ercode>");
																char errB = strBuffer.find("</ercode>");

																std::string strErr     = strBuffer.substr(errA+8,errB-(errA+8));

																cout << strErr << endl;
																cout << "******************" << strErr << "******************" << endl;
																int strint = std::stoi (strErr,nullptr,10);
																cout << "******************" << strint << "******************" << endl;

																// strErr >> ia;
																// cout << "--------int---------" << ia << "--------int-------" << endl;
																if(strint == -1)
																{
																								GPIO_OUT(LED_R,0);
																								GPIO_OUT(LED_G,1);
																								GPIO_OUT(LED_B,0);
																								transactionState = true;
																}
																else
																{
																								GPIO_OUT(LED_R,1);
																								GPIO_OUT(LED_G,0);
																								GPIO_OUT(LED_B,0);
																								transactionState = false;
																}


																if (close_connection(bio) == 0)
																								return 1;

																/* clean up the SSL context resources for the encrypted link */
																if (connection_type == 'e')
																								SSL_CTX_free(ctx);

																return 0;
								}

}
void Menu::init_symbols()
{
								//GPRS Symbol
								// system("cd ./bitmaps");
								// system("convert -depth 2 GPRS.bmp GPRS1.bmp");
								system("od -N 768 -j 138 -t x1 -v -w48 ./bitmaps/GPRS1_inv.bmp > ./bitmaps/GPRS1.txt");
								lcd.LCD::Init_symbolBMP("./bitmaps/GPRS1.txt", "GPRS");
								//WiFi Symbol
								// system("convert -depth 2 wifi.bmp wifi1.bmp");
								system("od -N 768 -j 138 -t x1 -v -w48 ./bitmaps/wifi1_inv.bmp > ./bitmaps/wifi1.txt");
								lcd.LCD::Init_symbolBMP("./bitmaps/wifi1.txt", "wifi");
								//Power Symbol
								// system("convert -depth 2 power.bmp power1.bmp");
								system("od -N 768 -j 138 -t x1 -v -w48 ./bitmaps/power1_inv.bmp > ./bitmaps/power1.txt");
								lcd.LCD::Init_symbolBMP("./bitmaps/power1.txt", "power");
								//Battery Symbol
								//  00% 20% 40% 60% 80% 99% 100%
								std::string batt_cap[7]  = {"0", "20", "40", "60", "80", "99", "100"};
								// std::string converter[3] = {"convert -depth 2 bat_",".bmp batt_1_",".bmp"};
								std::string textifier[3] = {"od -N 768 -j 138 -t x1 -v -w48 ./bitmaps/batt_1_","_inv.bmp > ./bitmaps/batt_1_",".txt"};
								std::string init_iser[3] = {"./bitmaps/batt_1_",".txt","batt_"};
								for(int i=0; i<7; i++)
								{
																// std::string stringer1 = converter[0] + batt_cap[i] + converter[1] + batt_cap[i] + converter[2];
																// system(stringer1.c_str());
																std::string stringer2 = textifier[0] + batt_cap[i] + textifier[1] + batt_cap[i] + textifier[2];
																system(stringer2.c_str());
																std::string stringer3 = init_iser[0] + batt_cap[i] + init_iser[1];
																std::string stringer4 = init_iser[2] + batt_cap[i];
																// std::cout << "given name: " << stringer4 << endl;
																lcd.LCD::Init_symbolBMP(stringer3.c_str(), stringer4.c_str());
								}
								// system("cd ..");
}

void Menu::update_symbols()
{
								lcd.lcd_clr_area(0, 0, 2, 16, 0);
								//GPRS Symbol
								std::string stat = exec("ip link | grep ppp");
								if (stat.find("ppp") != std::string::npos)
								{
																lcd.LCD::write_symbolBMP ("GPRS",0,0);
																// cout<<"printed: GPRS" << endl;
								}
								else lcd.LCD::lcd_put_big_str (0,63,"  ",0,0,0);
								//WiFi Symbol
								stat = exec("ip link | grep wlan");
								if (stat.find("state UP")!= std::string::npos)
								{
																lcd.LCD::write_symbolBMP ("wifi",0,16);
																// cout<<"printed: wifi" << endl;
								}
								else lcd.LCD::lcd_put_big_str (0,79,"  ",0,0,0);
								//Power Symbol
								int ret = readfileint("/sys/class/power_supply/ac/present");
								if(ret>0)
								{
																lcd.LCD::write_symbolBMP ("power",0,95);
																// cout<<"printed: power" << endl;
								}
								else lcd.LCD::lcd_put_big_str (0,95,"  ",0,0,0);
								//Battery Symbol
								ret = readfileint("/sys/class/power_supply/battery/present");
								int ret1 = readfileint("/sys/class/power_supply/battery/capacity");
								if(ret>0)
								{
																int ret2 = 0;
																for(int j=0; j<6; j++) {if(ret1>=(20*j)) ret2=(20*j); }
																if(ret1==99) ret2=ret1;
																string ret3 = "batt_";
																lcd.LCD::write_symbolBMP (ret3+std::to_string(ret2),0,111);
																// cout<<"printed: battery" << endl;
								}
								else lcd.LCD::lcd_put_big_str (0,111,"Er",0,0,0);
}

void Menu::screen_input(char input)
{
								if(input == '2') row = row-1;
								else if(input == '8') row = row+1;
								else if(input == '6') col = col+1;
								else if(input == '4') col = col-1;

								if(col<0) {col = 25; row -= 1; }
								if(col>25) {col = 0;  row += 1; }
								if(row>3) row =0;
								if(row<0) row =3;

								if(input == '5')
								{
																// lcd.lcd_clr_area(7, ((str.length()-1)*6), 1, 1);
																// str+=keypad.mapper( row, col);
								}
								else if(input == '0')
								{
																lcd.lcd_clr_area(7, ((str.length()-1)*6), 1, 1);
																str.erase((str.length()-1),1);
								}
								else
								{
																// if(str.length()>0)	str.erase((str.length()-1),1);
																// str+=keypad.mapper( row, col);
								}
								lcd.lcd_clr_area(6, 0, 1, (str.length()));
								lcd.lcd_put_str (6, 0, str);
								lcd.lcd_put_str (7, ((str.length()-1)*6), "^");
}

void Menu::main_loop()
{
								// lcd fun and games
								lcd.lcd_clr_all();
								// lcd.gray_test();
								// usleep(200000);
								// lcd.lcd_clr_all();

								//lcd.WiGroup_Logo();
								//time.sleep bla bla bla

								//TODO insert test pages and WiGroup Logo

								// foo  pointes to mem address of Home "address-of operator"
								foo=&Menu::Home;

								// every screen has an int function which uses this variable
								screen_update=0;

								IdleTimeStart = clock();
								// Each Menu AScreen function must update *foo!!
								while(true)
								{
																(this->*foo)();
								}
								return;
}

int main()
{
								Menu menu;
								menu.main_loop();
								return 0;
}
