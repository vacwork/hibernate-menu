Hibernation Menu
#################
Hibernation setup to attempt to avoid memory leaks during periods of inactivity on the wiReader device.
__________________________________________________________________________________________________________________

simplemenu - loaded on startup, after a period of inactivity the Idle menu is loaded,
  which then runs hibernate_load.sh after a further period of inactivity

hibernate_load.sh - kills all instances of simplemenu and clears the RAM + drivers before loading hibernatemenu

hibernatemenu - basic bitmap 'screensaver' is displayed while the system waits for any key to be pressed,
  when this occurs simple_load.sh is run

simple_menu.sh - kills all instances of hibernatemenu before loading simplemenu
